import React, { useEffect } from "react";
import { checkWin } from "../helpers/helpers";

const PopUp = ({ correctLetters, wrongLetters, selectedWord, setPlayable, playAgain }) => {
  let finalMessage = "";
  let finalMessageRevealWord = "";
  let playable = true;

  if (checkWin(correctLetters, wrongLetters, selectedWord) === "win") {
    finalMessage = "Congratulations! You are a Winner!";
    playable = false;
  } else if (checkWin(correctLetters, wrongLetters, selectedWord) === "lose") {
    finalMessage = "You had lost the game, try again!";
    finalMessageRevealWord = `The word was: ${selectedWord}`;
    playable = false;
  }

  useEffect(() => setPlayable(playable));

  return (
    <div
      className="popup-container"
      style={finalMessage !== "" ? { display: "flex" } : {}}
    >
      <div className="popup">
        <h2 id="final-message">{finalMessage}</h2>
        <h3 id="final-message-reveal-word">{finalMessageRevealWord}</h3>
        <button onClick={playAgain} id="play-button">Play Again</button>
      </div>
    </div>
  );
};

export default PopUp;
