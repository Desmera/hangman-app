import React from "react";

const Header = () => {
    return(
        <div className="header">
            <h1>The Hangman</h1>
            <p>Find the hidden work!</p>
        </div>
    )
}

export default Header;